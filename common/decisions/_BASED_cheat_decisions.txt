BASED_cheatmenu = {
    BASED_give_1_civ = {
        icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_offsite_building = { type = industrial_complex level = 1 }
		}
    }
    BASED_give_5_civ = {
        icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_offsite_building = { type = industrial_complex level = 5 }
		}
    }
    BASED_take_1_civ = {
        icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_offsite_building = { type = industrial_complex level = -1 }
		}
    }
    BASED_take_5_civ = {
        icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_offsite_building = { type = industrial_complex level = -5 }
		}
    }
    BASED_need_no_resourse = {
        icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_ideas = BASED_no_resourse
		}
    }
    BASED_need_resourse = {
        icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            remove_ideas = BASED_no_resourse
		}
    }
	BASED_add_900NIC = {
		icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_to_variable = {
				var = based_nuclear_points
				value = 900
			}
		}
	}
	BASED_add_2520NIC = {
		icon = generic_scorched_earth
		cost = 0
		ai_will_do = { factor = 0 }

		complete_effect = {
            add_to_variable = {
				var = based_nuclear_points
				value = 2520
			}
		}
	}
    create_operative = {
		icon = generic_research
		cost = 0
        ai_will_do = { base = 0 }
		complete_effect = {
			create_operative_leader = {
				bypass_recruitment = yes
			}
		}
	}
    create_recruitable_operative_debug = {
		icon = generic_research
		cost = 0
        ai_will_do = { base = 0 }
		complete_effect = {
			create_operative_leader = {
				bypass_recruitment = no
			}
		}
	}
}