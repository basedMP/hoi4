---------------
--MPP DEFINES--
---------------

--NGame
NDefines.NGame.GAME_SPEED_SECONDS = { 0.7 , 0.5, 0.2, 0.04, 0.0 } --horst speeds, but 5 is slower
NDefines.NGame.LAG_DAYS_FOR_LOWER_SPEED = 100
NDefines.NGame.LAG_DAYS_FOR_PAUSE = 50    							-- 
NDefines.NGame.COMBAT_LOG_MAX_MONTHS = 12				-- WAS 48 | drastically cuts down on save file sizes after WW2 starts and well into barbarossa
NDefines.NGame.MESSAGE_TIMEOUT_DAYS = 20					     	 -- WAS 60 	| less messages lying around at the top of your screen

--NAI
NDefines.NAI.DIPLOMACY_REJECTED_WAIT_MONTHS_BASE = 12                --up from 4 | should cut down on AI spam
NDefines.NAI.DIPLOMACY_ACCEPT_ATTACHE_BASE = 100;
NDefines.NAI.DIPLOMACY_ACCEPT_ATTACHE_OPINION_TRASHHOLD = 0;
NDefines.NAI.DIPLOMACY_ACCEPT_ATTACHE_OPINION_PENALTY = 0;

--NAir
NDefines.NAir.AIR_WING_MAX_SIZE = 1600                               -- this can be halved 4 times into 100 stacks (very convenient)
NDefines.NAir.AIR_WING_FLIGHT_SPEED_MULT = 0.2 --makes redeployement of fighters faster vanilla is 0.02
NDefines.NAir.AIR_DEPLOYMENT_DAYS = 0                              -- Down from 3 | Makes AC player much more responsive
NDefines.NAir.ACCIDENT_CHANCE_BASE = 0							-- Base chance % (0 - 100) for accident to happen. Reduced with higher reliability stat.
NDefines.NAir.ACCIDENT_CHANCE_CARRIER_MULT = 0					-- The total accident chance is scaled up when it happens on the carrier ship.
NDefines.NAir.ACCIDENT_CHANCE_BALANCE_MULT = 0					-- Multiplier for balancing how often the air accident really happens. The higher mult, the more often.
NDefines.NAir.ACCIDENT_EFFECT_MULT = 0					-- Multiplier for balancing the effect of accidents

--NCountry
NDefines.NCountry.MIN_FOCUSES_FOR_CONTINUOUS = 0                    -- WAS 10 | Why should you need 10 focuses to unlock continuous focuses???
NDefines.NCountry.AIR_VOLUNTEER_PLANES_LIMIT = 0 -- since u get 500 capped limit
NDefines.NCountry.UNCAPITULATE_LEVEL = 1                       -- VANILLA 0.1 if we reclaim this much and our capital we reset capitulate status
NDefines.NCountry.SPECIAL_FORCES_CAP_MIN = 336;
NDefines.NCountry.SPECIAL_FORCES_CAP_BASE = 0.00;
NDefines.NCountry.NUCLEAR_PRODUCTION_SCALE = 999; -- from 365, make nukes slower
NDefines.NCountry.GIE_CONVOY_ON_CREATION = 50                      -- WAS 10 | Changed so capped players dont instantly lose supply/ability to do invasions
NDefines.NCountry.COMPLIANCE_PER_COLLABORATION = 1.0 -- 100% * x = complince
NDefines.NCountry.COUNTRY_SCORE_MULTIPLIER = 0				-- Weight of the country score.
NDefines.NCountry.ARMY_SCORE_MULTIPLIER = 0					-- Based on number of armies.
NDefines.NCountry.NAVY_SCORE_MULTIPLIER = 0					-- Based on number of navies.
NDefines.NCountry.AIR_SCORE_MULTIPLIER = 0					-- Based on number of planes (which is typically a lot).
NDefines.NCountry.INDUSTRY_SCORE_MULTIPLIER = 0				-- Based on number of factories.
NDefines.NCountry.PROVINCE_SCORE_MULTIPLIER = 0				-- Based on number of controlled provinces.
NDefines.NCountry.POPULATION_YEARLY_GROWTH_BASE = 0               --Removed for game stability/reducing chance of desync
--NDefines.NCountry.GIE_HOST_CIC_FROM_LEGITIMACY_MAX = 0 					--Host will receive from 0 to this value in CIC.
--NDefines.NCountry.GIE_HOST_MIC_FROM_LEGITIMACY_MAX = 0					--Host will receive from 0 to this value in MIC.
--NDefines.NCountry.STATE_VALUE_BUILDING_SLOTS_MULT = 0			-- The Value of each building slot in a state ( Value is used to determine costs in peace conference and PP cost to occupy )
--NDefines.NCountry.STATE_OCCUPATION_COST_MULTIPLIER = 0		-- Multiplier for state value to PP cost for occupation
--NDefines.NCountry.ARMY_IMPORTANCE_FACTOR = 0                   -- Army factor for AI and calculations
--NDefines.NCountry.TERRAIN_IMPORTANCE_FACTOR = 0               -- Terrain base factor for state strategic value
--NDefines.NCountry.VICTORY_POINTS_IMPORTANCE_FACTOR = 0           -- State victory points importance factor for AI and calculations
--NDefines.NCountry.BUILDING_IMPORTANCE_FACTOR = 0               -- State building importance factor for AI and calculations
--NDefines.NCountry.RESOURCE_IMPORTANCE_FACTOR = 0               -- State resource importance factor for AI and calculations
--NDefines.NCountry.FRONT_PROVINCE_SCORE = 0   					-- Max province score of a front. Used for the hostile troop alert

--NDiplomacy
NDefines.NDiplomacy.VOLUNTEERS_PER_TARGET_PROVINCE = 0      -- since u get 5 capped limit
NDefines.NDiplomacy.VOLUNTEERS_PER_COUNTRY_ARMY = 0
NDefines.NDiplomacy.VOLUNTEERS_DIVISIONS_REQUIRED = 0       -- WAS 30 | This many divisions are required for the country to be able to send volunteers.
NDefines.NDiplomacy.VOLUNTEERS_RETURN_EQUIPMENT = 1		    -- Returning volunteers keep this much equipment
NDefines.NDiplomacy.FRONT_IS_DANGEROUS = 0					-- AI doesn't care if a front is dangerous
NDefines.NDiplomacy.MIN_TRUST_VALUE = -500 					-- WAS -100 | Min opinion value cap.
NDefines.NDiplomacy.MAX_TRUST_VALUE = 500 					-- WAS 100 | MAX opinion value cap.
NDefines.NDiplomacy.DIPLOMACY_HOURS_BETWEEN_REQUESTS = 12   -- Cuts annoying spam from players like WestWood ~Thrasymachus
NDefines.NDiplomacy.TROOP_FEAR = 0 							-- Impact on troops on borders when deciding how willing a nation is to trade
NDefines.NDiplomacy.FLEET_FEAR = 0							-- Impact on troops on borders when deciding how willing a nation is to trade
NDefines.NDiplomacy.ATTACHE_XP_SHARE = 0.25                 --as requested by discord server
NDefines.NDiplomacy.PEACE_SCORE_PER_PASS = 100.0  -- When you pass once you should get enough points to finish the peace deal

--NTrade
NDefines.NTrade.ANTI_MONOPOLY_TRADE_FACTOR = 0				-- WAS -100 this group reduces the number of opinion/trade factor changes the game tracks| This is added to the factor value when anti-monopoly threshold is exceeded; cucks majors often if the value is vanilla
NDefines.NTrade.PARTY_SUPPORT_TRADE_FACTOR = 0			    -- Trade factor bonus at the other side having 100 % party popularity for my party
NDefines.NTrade.ANTI_MONOPOLY_TRADE_FACTOR_THRESHOLD = 0	-- What percentage of resources has to be sold to the buyer for the anti-monopoly factor to take effect
NDefines.NTrade.MAX_MONTH_TRADE_FACTOR = 0				    -- This is the maximum bonus that can be gained from time
NDefines.NTrade.DISTANCE_TRADE_FACTOR = 0				    -- Trade factor is modified by distance times this
NDefines.NTrade.RELATION_TRADE_FACTOR = 0				    -- Trade factor is modified by Opinion value times this

--NMilitary
NDefines.NMilitary.UNIT_LEADER_MODIFIER_COOLDOWN_ON_GROUP_CHANGE = 0    -- WAS 15 | This prevents reassignment memes | This is the number of days it takes to REASSIGN a general. 
NDefines.NMilitary.BASE_DIVISION_BRIGADE_GROUP_COST = 0                 -- 0xp, was 25
NDefines.NMilitary.BASE_DIVISION_BRIGADE_CHANGE_COST = 0                -- 0xp, was 5
NDefines.NMilitary.BASE_DIVISION_SUPPORT_SLOT_COST = 0                  -- 0xp, was 10
NDefines.NMilitary.MAX_ARMY_EXPERIENCE = 999;
NDefines.NMilitary.MAX_NAVY_EXPERIENCE = 999;
NDefines.NMilitary.MAX_AIR_EXPERIENCE  = 999;
NDefines.NMilitary.ARMY_LEADER_XP_GAIN_PER_UNIT_IN_COMBAT = 0.11
NDefines.NMilitary.GARRISON_ORDER_ARMY_CAP_FACTOR = 4.2             --was 3
NDefines.NMilitary.ENCIRCLED_DISBAND_MANPOWER_FACTOR = 0            -- WAS 0.2 | Most rulesets ban deleting encircled troops, but at least this prevents some manpower from returning | Deleting encircled divisions is always banned anyways, so this reduces unfair play | percentage of manpower returned when an encircled unit is disbanded
NDefines.NMilitary.MISSION_COMMAND_POWER_COSTS = {
    0.0, -- AIR_SUPERIORITY
    0.0, -- CAS
    0.0, -- INTERCEPTION
    0.0, -- STRATEGIC_BOMBER
    0.0, -- NAVAL_BOMBER
    0.0, -- DROP_NUKE
    0.0, -- PARADROP
    0.0, -- NAVAL_KAMIKAZE
    0.0, -- PORT_STRIKE
    0.15, -- AIR_SUPPLY
    0.0, -- TRAINING
    0.0, -- NAVAL_MINES_PLANTING
    0.0, -- NAVAL_MINES_SWEEPING
    0.0  -- MISSION_RECON
}

NDefines.NMilitary.NUKE_DELAY_HOURS = 48;                   -- one nuke per 2 days
NDefines.NMilitary.NUKE_MIN_DAMAGE_PERCENT = 0.95
NDefines.NMilitary.NUKE_MAX_DAMAGE_PERCENT = 1.0 --nuke will kill divisions
NDefines.NMilitary.DEPLOY_TRAINING_MAX_LEVEL = 2;
NDefines.NDeployment.BASE_DEPLOYMENT_TRAINING = 0.75;

--NNavy
NDefines.NNavy.TRAINING_ACCIDENT_CHANCES = 0                -- down from 0.02 | Chances one ship get damage each hour while on training 
NDefines.NNavy.RESOURCE_EXPORT_PRIORITY = 3;
NDefines.NNavy.RESOURCE_LENDLEASE_PRIORITY = 3;
NDefines.NNavy.RESOURCE_ORIGIN_PRIORITY = 3;
NDefines.NNavy.NAVAL_MINES_IN_REGION_MAX = 1;
NDefines.NNavy.SUBMARINE_BASE_TORPEDO_REVEAL_CHANCE = 0.04  -- from 0.035, makes subs visible w/ higher chance when shooting
NDefines.NNavy.SUBMARINE_REVEAL_BASE_CHANCE = 12            -- the funny, from 11
NDefines.NNavy.ADMIRAL_TASKFORCE_CAP = 40                   -- WAS 10 | Increased so players can use their same submarine or escort admiral without penalties
NDefines.NNavy.MISSION_SUPREMACY_RATIOS = {
    0.0, -- HOLD;
    1.0, -- PATROL;
    1.0, -- STRIKE FORCE; will prob be changed soon:tm:
    0.0, -- CONVOY RAIDING;
    0.0, -- CONVOY ESCORT;
    0.0, -- MINES PLANTING;
    0.0, -- MINES SWEEPING;
    0.0, -- TRAIN;
    0.0, -- RESERVE_FLEET;
    1.0  -- NAVAL_INVASION_SUPPORT
}

--NFocus
NDefines.NFocus.MAX_SAVED_FOCUS_PROGRESS = 20   --up from 10, should allow for more flexibility with picking focuses while doing something else, like tank templates

--NPolitics
NDefines.NPolitics.ARMY_LEADER_COST = 1	    -- VANILLA 5 | cost for recruiting new leaders, 'this value' * number_of_existing_leaders_of_type

--NProduction
NDefines.NProduction.MIN_LICENSE_ACTIVE_DAYS = 0                    -- License can be cancelled at any time now, down from 30
NDefines.NProduction.MIN_POSSIBLE_TRAINING_MANPOWER = 10000000      -- Increased so most nations don't need to queue up multiple lines of infantry or spam 2w infantry and convert
NDefines.NProduction.CONVOY_MAX_NAV_FACTORIES_PER_LINE = 300        -- WAS 15, Changed so you can have less lines of naval production

NDefines.NProduction.EQUIPMENT_MODULE_ADD_XP_COST = 0.0;
NDefines.NProduction.EQUIPMENT_MODULE_REPLACE_XP_COST = 0.0;
NDefines.NProduction.EQUIPMENT_MODULE_CONVERT_XP_COST = 0.0;
NDefines.NProduction.EQUIPMENT_MODULE_REMOVE_XP_COST = 0.0;

NDefines.NProduction.BASE_FACTORY_SPEED_NAV = 5 --from 2.5, ships go brr

NDefines.NProduction.BASE_LICENSE_IC_COST = 0;
NDefines.NProduction.LICENSE_IC_COST_YEAR_INCREASE = 0;




--NOperatives
NDefines.NOperatives.AGENCY_UPGRADE_PER_OPERATIVE_SLOT = 1 --get 2nd one after one upgrade

--NGraficks
NDefines.NGraphics.COUNTRY_COLOR_SATURATION_MODIFIER = 0.8 -- Vanilla 0.6   HFU 0.9

NDefines_Graphics.NGraphics.MAPICON_GROUP_PASSES = 50
NDefines_Graphics.NGraphics.MAP_ICONS_COARSE_COUNTRY_GROUPING_DISTANCE = 200
NDefines_Graphics.NGraphics.MAP_ICONS_COARSE_COUNTRY_GROUPING_DISTANCE_STRATEGIC = 0

--NDEFINES_GRAFIKS
NDefines_Graphics.NAirGfx.MAX_MISSILE_BOMBING_SCENARIOS = 1
NDefines_Graphics.NAirGfx.MAX_BOMBING_SCENARIOS = 1
NDefines_Graphics.NAirGfx.MAX_PATROL_SCENARIOS = 1
NDefines_Graphics.NAirGfx.MAX_DOGFIGHTS_SCENARIOS = 1
NDefines_Graphics.NAirGfx.MAX_TRANSPORT_SCENARIOS = 1
NDefines_Graphics.NAirGfx.AIRPLANES_ANIMATION_GLOBAL_SPEED_PER_GAMESPEED = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1}
NDefines_Graphics.NAirGfx.AIRPLANES_SMOOTH_INTERPOLATION_MOVE = 0.02
NDefines_Graphics.NAirGfx.AIRPLANES_SMOOTH_INTERPOLATION_TURN = 0.02
NDefines_Graphics.NMapMode.MAP_MODE_TERRAIN_TRANSPARENCY = 0.8

NDefines_Graphics.NGraphics.COMMANDGROUP_PRESET_COLORS_HSV = {
	0.0/360.0, 1.0, 0.75,	--red
	10.0/360.0, 1.0, 0.75,	--orange
	60.0/360.0, 1.0, 0.75,	--yellow
	120.0/360.0, 0.85, 0.75,	--green
	155.0/360.0, 1.0, 0.75,	--greenish
	180.0/360.0, 1.0, 0.75,	--turq
	220.0/360.0, 1.0, 0.75,	--blue
	260.0/360.0, 1.0, 0.85,	--dark purple
	330.0/360.0, 0, 0.75		--white
}