GER	= "countries/Germany.txt"
ENG = "countries/United Kingdom.txt"
SOV = "countries/Soviet Union.txt"
SWE	= "countries/Sweden.txt"
FRA	= "countries/France.txt"
LUX	= "countries/Luxemburg.txt"
BEL	= "countries/Belgium.txt"
HOL	= "countries/Holland.txt"
CZE	= "countries/Czechoslovakia.txt"
POL	= "countries/Poland.txt"
AUS	= "countries/Austria.txt"
LIT	= "countries/Lithuania.txt"
EST	= "countries/Estonia.txt"
LAT	= "countries/Latvia.txt"
SPR = "countries/Spain.txt"
ITA = "countries/Italy.txt"
ROM = "countries/Romania.txt"
YUG = "countries/Yugoslavia.txt"
SWI = "countries/Switzerland.txt"
TUR = "countries/Turkey.txt"
GRE = "countries/Greece.txt"
NOR = "countries/Norway.txt"
DEN = "countries/Denmark.txt"
BUL = "countries/Bulgaria.txt"
FIN = "countries/Finland.txt"
IRE = "countries/Ireland.txt"
HUN = "countries/Hungary.txt"
ARG = "countries/Argentina.txt"
AST = "countries/Australia.txt"
BRA = "countries/Brazil.txt"
CAN = "countries/Canada.txt"
CHI = "countries/China.txt"
ETH = "countries/Ethiopia.txt"
IRQ = "countries/Iraq.txt"
JAP = "countries/Japan.txt"
MEN = "countries/Mengkukuo.txt"
NZL = "countries/New Zealand.txt"
PER = "countries/Persia.txt"
SAF = "countries/South Africa.txt"
SAU = "countries/Saudi Arabia.txt"
SIA = "countries/Siam.txt"
VEN = "countries/Venezula.txt"
USA = "countries/USA.txt"
MON = "countries/Mongolia.txt"
CUB = "countries/Cuba.txt"
RAJ = "countries/British Raj.txt"
ICE = "countries/Iceland.txt"
PAK = "countries/Pakistan.txt"
MAN = "countries/Manchukou.txt"
BAN = "countries/Bangladesh.txt"
BRM = "countries/Burma.txt"
HAW = "countries/Hawaii.txt"
INS = "countries/Indonesia.txt"
MEX = "countries/Mexico.txt"
