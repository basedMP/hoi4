# based MP mod, except its on gitlab
just so dumm slayer and uxelduxel can coop switzerland
mod id is: > 2635500693

# todo (bcs fink removes rights every 3 secs moved to here)
- localistaion bs
- axis minors faster research after 40.5 (4)
- SAF remove sudan takeover
- ita more dockyards
- redo localisation (tanks, lind)

# idea shit
- free market++ 100% (?)
- kz/gulag stuff
- gas attacks
- generals weniger/mehr gameimpact? (Traits shit)
- air upgrades changes (mehr stufen?, weniger pro stufe, weniger stufen)
- recall volunteers
- less ressoucen/oil
- SOV winter war bs

- ROM ANNEX HUN, CZE DOESNT EXIST + fasco
- SPR ANNEX POR, SCW HAS ENED
- SOV eastern poland demander yeet

## hardcivved, no consumer goods %:
 * civs for stability
 * civs for research speed
 * civs for propaganda/war support 

# All hoi4 nations, if someones interested
Germany
United Kingdom
Soviet Union
Sweden
France
Luxemburg
Belgium
Holland
Czechoslovakia
Poland
Austria
Lithuania
Estonia
Latvia
Spain
Italy
Romania
Yugoslavia
Serbia
Switzerland
Turkey
Greece
Albania
Norway
Denmark
Bulgaria
Portugal
Finland
Ireland
Hungary
Afghanistan
Argentina
Australia
Bhutan
Bolivia
Brazil
Canada
China
Chile
Colombia
Costa Rica
Ecuador
El Salvador
Ethiopia
Guatemla
Honduras
Iraq
Japan
Korea
Liberia
Mexico
Mengkukuo
Nepal
Nicaragua
New Zealand
Panama
Persia
Philippines
Peru
South Africa
Saudi Arabia
Siam
Sinkiang
Tibet
Uruguay
Venezula
Yunnan
USA
Mongolia
Tannu Tuva
Paraguay
Cuba
Dominican Republic
Haiti
Yeman
Oman
Slovakia
British Raj
Croatia
Guangxi
ComChina
Shanxi
Xibei San Ma
Iceland
Lebanon
Jordan
Syria
Egypt
Libya
West Germany
East Germany
Palestine
Israel
Vietnam
Cambodia
Malaysia
Indonesia
Laos
Montenegro
Ukraine
Georgia
Kazakhstan 
Azerbaijan
Armenia
Belarus
Angola
Mozambique
Zimbabwe
Congo
Kenya
Pakistan
Botswana
Manchukou
Bahamas
Bangladesh
Belize
Burma
Curacao
Guadeloupe
Guyana
Jamaica
Kyrgyzstan
Madagascar
Moldova
Papua New Guinea
Puerto Rico
Qatar
Scotland
Sri Lanka
Suriname
Tajikistan
Trinidad and Tobago
Turkmenistan
United Arab Emirates
Uzbekistan
Kuwait
Cyprus
Malta
Algeria
Morocco
Tunisia
Sudan
Eritrea
Djibouti
Somalia
Uganda
Rwanda
Burundi
Tanzania
Zambia
Malawi
Gabon
Republic of Congo
Equatorial Guinea
Cameroon
Central African Republic
Chad
Nigeria
Dahomey
Togo
Ghana
Ivory Coast
Upper Volta
Mali
Sierra Leone
Guinea
Guinea-Bissau
Senegal
Gambia
Wales
Niger
CSA
USB#neutral US bloc during civil war in MTG
Mauritania
Namibia
Western Sahara
British Antilles
Cayenne
Maldives
Fiji
Micronesia
Tahiti
Guam
Solomon
Samoa
Hawaii
Slovenia
Bosnia
Herzegovina
Macedonia
Northern Ireland
Bavaria
Mecklenburg
Prussia
Saxony
Hanover
Wurtemberg
Schleswig-Holstein
Catalonia
Navarra
Galicia
Andalusia
Brittany
Occitania
Corsica
Kurdistan
Transylvania
Danzig
Silesia
Kashubia
Don Republic
Kuban Republic
Bukharan Republic
Altay
Kalmykia
Karelia
Crimea
Tatarstan
Chechnya Ingushetia
Dagestan
Buryatia
Chukotka
Fareastern Republic
Yakutia
Vladivostok
Karakalpakstan
Yamalia
Taymyria
Ostyak Vogulia
Nenetsia
Komi
Abkhazia
Kabardino Balkaria
North Ossetia
Volga Germany
Bashkortostan
Khiva
Udmurtia
Chuvashia
Mariel
Kosovo

# (big) Changes

## General
- Added Playerled Peace
- removed 150 Nations for Gamespeed
- Custom Game Defines for Performance
- Removed much useless Gamecode

## Politics
- Noone can guarantee, set default Guarantees
 + UK/FRA on POL
 + USA on American Sphere + Philippines
- GER can Reichskommissariat (if AI):
 + HUN
 + ROM
 + BUL
- ENG can Integrate (if AI):
 + CAN
 + AST
 + NZL
 + SAF
- ROM can annex BUL if fasc
- JAP can annex MEN,MAN,SIA (if AI)

## Focusses
- based on HMM
- SAF:
 + can do both sides of tree
- GER:
 + Streamlined + Arty Buffs
- SOV:
 + faster Purge + Tank Buffs
- SPR:
 + can get Whole Iberia if done Stamp out Maquis
 + can go Phalanx
- Generic:
 + no Kamikaze
 + one less bonus slot
 + only 1 year ahead for nukes
- and much other stuff

## Warfare
- Air
 + Max Air Wing Size 1600 (4 Splits is 16*1000)
- Navy
 + Removed Minelaying

## Production
- Doubled Dockyard Output for Historically more Ships and giving it more Skill Potential

## Research
- Added 1 Slot for everyone
- default Researched Fuel Silos, Naval Invasion 1, Trains, Support Companies
- Removed Strats except 1
- Sub3 is 1942, Sub4 removed

## Graphics
- Added Custom Icons for Economy Laws
- Added Custom Menu