ccodes = [ "GER", "ENG", "SOV", "SWE", "FRA", "LUX", "BEL", "HOL", "CZE", "POL", "AUS", "LIT", "EST", "LAT", "SPR", "ITA", "ROM", "YUG", "SWI", "TUR", "GRE", "ALB", "NOR", "DEN", "BUL", "SWE", "POR", "FIN", "IRE", "HUN", "ARG", "AST", "BRA", "CAN", "CHI", "ETH", "IRQ", "JAP", "MEX", "MEN", "NZL", "PER", "PHI", "SAF", "SAU", "SIA", "VEN", "USA", "MON", "TAN", "CUB", "RAJ", "ICE", "PAK", "BAN", "BRM", "HAW", "INS"]
import os
dir_list = os.listdir("./history/states")
for i in dir_list:
    foil = ""
    a = open("./history/states/"+i, "r", encoding="UTF-8").readlines()
    for line in a:
        if "add_core_of" in line:
            for c in ccodes:
                if c in line:
                    foil += line
                else:
                    print(i + ": " + line)
        else:
            foil += line
    open("./history/states2/"+i,"w+", encoding="UTF-8").write(foil)