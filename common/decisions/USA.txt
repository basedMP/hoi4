war_measures = {
	USA_homeland_defense = {

		icon = generic_prepare_civil_war

		allowed = {
			original_tag = USA
		}

		available = {
			any_state = {
				is_core_of = USA
				NOT = {
					is_controlled_by = USA
				}
			}
			has_civil_war = no

		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 200
		}

		visible = {
			has_defensive_war = yes
			CSA = {
				exists = no
			}
		}

		complete_effect = {
			if = {
				limit = {
					has_war_support < 0.9
				}
				set_war_support = 0.9
			}
			if = {
				limit = {
					has_idea = great_depression
				}
				remove_ideas = great_depression
			}
			if = {
				limit = {
					NOT = {
						OR = {
							has_idea = extensive_conscription
							has_idea = scraping_the_barrel
							has_idea = service_by_requirement
							has_idea = all_adults_serve
						}
					}
				}
				add_ideas = extensive_conscription
			}
			if = {
				limit = {
					OR = {
						has_idea = undisturbed_isolation
						has_idea = isolation
						has_idea = civilian_economy
						has_idea = low_economic_mobilisation
					}
				}
				add_ideas = war_economy
			}
			add_ideas = USA_homeland_defense
		}
	}
}
USA_aid_britain = {
	USA_establish_personal_communication_with_former_naval_person = {

		icon = generic_political_discourse

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = blood_toil_tears_sweat_speech
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 60 }
			}
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 99 }
			}
		}

		visible = {
			has_war = no
		}

		complete_effect = {
			add_war_support = 0.05
			add_opinion_modifier = { target = ENG modifier = USA_special_relationship }
			reverse_add_opinion_modifier = { target = ENG modifier = USA_special_relationship }
		}
	}

	USA_battle_domestic_isolationism = {

		icon = generic_civil_support

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = fight_on_the_beaches_speech
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 60 }
			}
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 99 }
			}
		}

		modifier = {
			war_support_weekly = 0.002		
		}

		days_remove = 180

		visible = {
			has_war = no
		}

		complete_effect = {
			add_war_support = 0.05
		}
	}

	USA_emergency_arms_deliveries = {

		icon = generic_prepare_civil_war

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = this_was_their_finest_hour_speech
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 60 }
			}
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 99 }
			}
		}

		modifier = {
			war_support_weekly = 0.005		
		}

		days_remove = 30

		visible = {
			has_war = no
		}

		complete_effect = {
			ENG = {
				add_equipment_to_stockpile = { type = infantry_equipment_0 amount = 20000 producer = USA }
				add_equipment_to_stockpile = { type = artillery_equipment_1 amount = 300 producer = USA }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 300 producer = USA }
				country_event = { id = wtt_britain.30 }
			}
		}
	}

	USA_arsenal_of_democracy_decision = {

		icon = generic_industry

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = mers_el_kebir_raid
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 10
		}

		visible = {
			has_war = no
		}

		modifier = {
			war_support_weekly = 0.001		
			industrial_capacity_factory = 0.02
			industrial_capacity_dockyard = 0.02
			consumer_goods_factor = -0.05
		}

		days_remove = 360

		complete_effect = {
		}
	}
}

foreign_support = {
	USA_support_the_anti_fascist_war = {

		icon = generic_industry

		allowed = {
			original_tag = USA
		}

		available = {
			NOT = {
				has_global_flag = USA_support_the_anti_fascist_war_flag
			}
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_war_with = JAP
			}
		}

		targets = { CHI }
		targets_dynamic = yes

		target_root_trigger = {
			OR = {
				has_completed_focus = USA_limited_intervention
				has_completed_focus = USA_lend_lease_act
			}
		}

		target_trigger = {
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_war_with = JAP
			}
		}

		visible = {
			has_government = democratic
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_war_with = JAP
			}
		}

		cost = 25
		ai_will_do = {
			factor = 50
			modifier = {
				factor = 3
				any_other_country = {
					has_completed_focus = CHI_mission_to_the_us
					has_idea = CHI_soong_mei_ling
					surrender_progress > 0.2
					has_defensive_war = yes
				}
			}

			modifier = {
				factor = 0.5
				has_war_support > 0.6
			}

			modifier = {
				factor = 2
				has_war_with = JAP
			}
		}

		fire_only_once = no

		modifier = {
			civilian_factory_use = 5			
		}

		days_remove = 180

		complete_effect = {
			FROM = {
				add_offsite_building = { type = arms_factory level = 3 }
				set_global_flag = USA_support_the_anti_fascist_war_flag
			}
		}

		remove_effect = {
			add_war_support = 0.05
			FROM = {
				add_offsite_building = { type = arms_factory level = -3 }
				hidden_effect = {
					clr_global_flag = USA_support_the_anti_fascist_war_flag
				}
			}
		}
	}

	USA_guns_for_the_anti_fascist_war = {

		icon = generic_prepare_civil_war

		allowed = {
			original_tag = USA
		}

		available = {
			has_equipment = { infantry_equipment > 999 }
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_idea = CHI_soong_mei_ling
				has_war_with = JAP
			}
		}

		targets = { CHI }
		targets_dynamic = yes

		target_trigger = {
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_idea = CHI_soong_mei_ling
				has_war_with = JAP
			}
		}

		visible = {
			has_government = democratic
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_idea = CHI_soong_mei_ling
				has_war_with = JAP
			}
		}

		cost = 25
		ai_will_do = {
			factor = 25
			modifier = {
				factor = 3
				FROM = {
					has_completed_focus = CHI_mission_to_the_us
					has_idea = CHI_soong_mei_ling
					surrender_progress > 0.2
					has_defensive_war = yes
				}
			}

			modifier = {
				factor = 0.5
				has_war_support < 0.6
			}

			modifier = {
				factor = 2
				has_war_with = JAP
			}
		}

		fire_only_once = no	

		days_re_enable = 180

		complete_effect = {
			ROOT = {
				send_equipment = {
					target = FROM
					type = infantry_equipment
					amount = 1000
					old_prioritised = yes
				}
			}
		}
	}
}
USA_foreign_support = {
	USA_invite_donations_FROM = {
		icon = ger_mefo_bills
		available = {
			has_war = no
		}
		visible = {
			has_completed_focus = USA_invite_foreign_support
			has_war = no
		}
		target_trigger = {
			FROM = {
				is_major = yes
				exists = yes
				has_war = no
				has_government = fascism
			}
		}
		cost = 25
		ai_will_do = {
			factor = 1
		}
		days_re_enable = 45
		complete_effect = {
			FROM = {
				country_event = mtg_usa_foreign_support.1
			}
		}
	}
	
	USA_smuggle_weapons_FROM = {
		icon = generic_prepare_civil_war
		available = {
			has_war = no
		}
		visible = {
			has_completed_focus = USA_invite_foreign_support
			has_war = no

		}
		target_trigger = {
			FROM = {
				exists = yes
				is_major = yes
				has_government = fascism
				has_war = no
				has_country_flag = USA_foreign_support_donations_approved
			}
		}
		cost = 50
		ai_will_do = {
			factor = 1
		}
		days_re_enable = 45
		complete_effect = {
			FROM = {
				country_event = mtg_usa_foreign_support.4
			}
		}
	}
	USA_training_camps_in_FROM = {
		icon = generic_prepare_civil_war
		available = {
			has_war = no
		}
		visible = {
			has_completed_focus = USA_invite_foreign_support
			has_war = no
		}
		target_trigger = {
			FROM = {
				is_major = yes
				exists = yes
				has_war = no
				has_government = fascism
				not = { has_country_flag = USA_training_camps_approved }
				has_country_flag = USA_foreign_support_donations_approved
			}
		}
		cost = 75
		ai_will_do = {
			factor = 1
		}
		days_re_enable = 45
		complete_effect = {
			FROM = {	
				country_event = mtg_usa_foreign_support.9
			}
		}
	}
	USA_pilot_training_in_FROM = {
		icon = generic_air
		available = {
			has_war = no
		}
		visible = {
			has_completed_focus = USA_invite_foreign_support
			has_war = no
		}
		target_trigger = {
			FROM = {
				exists = yes
				is_major = yes
				has_war = no
				has_government = fascism
				has_tech = fighter1
				not = { has_country_flag = usa_pilot_training }
				has_country_flag = USA_foreign_support_donations_approved
			}
		}
		cost = 75
		ai_will_do = {
			factor = 1
		}
		days_re_enable = 45
		complete_effect = {
			FROM = {
				set_country_flag = usa_pilot_training
				country_event = mtg_usa_foreign_support.12
			}
		}
	}
	USA_fund_shipyards_FROM = {
		icon = generic_construction
		available = {
			has_war = no
		}
		visible = {
			has_completed_focus = USA_invite_foreign_support
			has_war = no
		}
		target_trigger = {
			FROM = {
				exists = yes
				is_major = yes
				has_war = no
				has_government = fascism
				has_country_flag = USA_foreign_support_donations_approved
			}
		}
		cost = 125
		ai_will_do = {
			factor = 1
		}
		days_re_enable = 90
		complete_effect = {
			FROM = {
				country_event = mtg_usa_foreign_support.15
			}
		}
	}
}

economy_decisions = {
	USA_communal_domain = {
		icon = generic_operation
		available = {
			any_controlled_state = {
				is_core_of = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
		}
		visible = {
			has_completed_focus = USA_communal_property_act
		}
		cost = 50
		ai_will_do = {
			factor = 1
		}
		days_re_enable = 90

		complete_effect = {
			random_owned_controlled_state = {
				limit = {
					free_building_slots = {
						building = industrial_complex
						size > 1
						include_locked = yes
					}
				}
				add_extra_state_shared_building_slots = 1
			}
		}
	}
}

USA_war_plans = {
	USA_execute_war_plan_silver = {
		icon = generic_operation
		available = {
			has_war_with = ITA
		}
		visible = {
			has_completed_focus = USA_war_plan_silver
		}
		ai_will_do = {
			factor = 1
		}
		fire_only_once = yes
		days_remove = 180
		targeted_modifier = {
			tag = ITA
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
	}
	USA_execute_war_plan_orange = {
		icon = generic_operation
		available = {
			has_war_with = JAP
		}
		visible = {
			has_completed_focus = USA_war_plan_orange
		}
		ai_will_do = {
			factor = 1
		}
		fire_only_once = yes
		days_remove = 180
		modifier = {
			amphibious_invasion = 0.1
			invasion_preparation = -0.25
		}
		targeted_modifier = {
			tag = JAP
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
		targeted_modifier = {
			tag = MAN
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
		targeted_modifier = {
			tag = MEN
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
	}
	USA_execute_war_plan_black = {
		icon = generic_operation
		available = {
			has_war_with = GER
		}
		visible = {
			has_completed_focus = USA_war_plan_black
		}
		ai_will_do = {
			factor = 1
		}
		fire_only_once = yes
		days_remove = 180
		targeted_modifier = {
			tag = GER
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
	}
}
USA_congress = {
	USA_small_lobby_effort = {
		icon = generic_political_discourse
		available = {
			OR = {
				check_variable = { senators_support < senators_total }
				check_variable = { representatives_support < representatives_total }
			}
			not = {
				has_decision = USA_medium_lobby_effort
			}
			OR = {
				political_power_daily > 0.3
				has_political_power > 10
			}
		}
		cost = 0
		ai_will_do = {
			factor = 10
			modifier = {
				congress_medium_support_trigger = yes
				factor = 0.5
			}
			modifier = {
				congress_low_support_trigger = yes
				factor = 0.5
			}
			modifier = {
				congress_high_support_trigger = yes
				factor = 0
			}
		}
		modifier = {
			political_power_gain = -0.3
		}
		days_remove = 30
		remove_effect = {
			USA_congress_small_support = yes
		}
	}
	USA_medium_lobby_effort = {
		icon = generic_political_discourse
		available = {
			OR = {
				check_variable = { senators_support < senators_total }
				check_variable = { representatives_support < representatives_total }
			}
			not = {
				has_decision = USA_small_lobby_effort
			}
			OR = {
				political_power_daily > 0.75
				has_political_power > 35
			}
		}
		cost = 0
		ai_will_do = {
			factor = 10
			modifier = {
				congress_medium_support_trigger = yes
				factor = 0.5
			}
			modifier = {
				congress_low_support_trigger = yes
				factor = 0.5
			}
			modifier = {
				congress_high_support_trigger = yes
				factor = 0
			}
		}
		days_remove = 45
		modifier = {
			political_power_gain = -0.75
		}
		remove_effect = {
			USA_congress_medium_support = yes
		}
	}
	USA_special_measures = {
		icon = ger_mefo_bills
		available = {
			OR = {
				check_variable = { senators_support < senators_total }
				check_variable = { representatives_support < representatives_total }
			}
		}
		cost = 50
		ai_will_do = {
			factor = 100
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		days_re_enable = 90
		complete_effect = {
			USA_congress_small_support = yes
		}
	}
	USA_use_huac = {
		icon = oppression
		available = {
			has_idea = USA_joseph_mccarthy
			OR = {
				check_variable = { senators_support < senators_total }
				check_variable = { representatives_support < representatives_total }
			}
		}
		visible = {
			has_completed_focus = USA_empower_the_huac
		}
		cost = 10
		ai_will_do = {
			factor = 1
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		days_re_enable = 90
		complete_effect = {
			USA_congress_small_support = yes
			add_stability = -0.05
		}
	}
	USA_beat_up_opposition = {
		icon = oppression
		available = {
			has_government = democratic
		}
		visible = { has_completed_focus = USA_ally_with_the_silver_shirts }
		cost = 10
		ai_will_do = {
			factor = 1
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		days_re_enable = 90
		complete_effect = {
			add_stability = -0.1
			USA_congress_small_support = yes
			add_popularity = { ideology = fascism popularity = 0.02 }
		}
	}
	USA_pay_farm_subsidies = {
		icon = ger_mefo_bills
		available = {
			has_government = democratic
			OR = {
				check_variable = { senators_support < senators_total }
				check_variable = { representatives_support < representatives_total }
			}
		}
		visible = { has_completed_focus = USA_agricultural_adjustment_act }
		cost = 0
		ai_will_do = {
			factor = 1
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		days_remove = 90
		modifier = { consumer_goods_factor = 0.03 }
		remove_effect = {
			USA_congress_small_support = yes
		}
	}
	USA_give_tax_break = {
		icon = ger_mefo_bills
		available = {
			has_government = democratic
			OR = {
				check_variable = { senators_support < senators_total }
				check_variable = { representatives_support < representatives_total }
			}
		}
		visible = { has_completed_focus = USA_income_tax_reform }
		cost = 0
		ai_will_do = {
			factor = 1
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		days_remove = 90
		modifier = { consumer_goods_factor = 0.03 }
		remove_effect = {
			USA_congress_small_support = yes
		}
	}
	USA_amend_the_budget = {
		icon = ger_mefo_bills
		available = {
			congress_medium_support_trigger = yes
		}
		visible = {has_government = democratic }
		cost = 25
		days_remove = 90
		days_re_enable = 275
		ai_will_do = {
			factor = 1
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		modifier = { consumer_goods_factor = -0.05 }
		remove_effect = {
			USA_congress_medium_opposition = yes
		}
	}
	USA_research_grants = {
		icon = ger_mefo_bills
		available = {
			congress_medium_support_trigger = yes
		}
		visible = {has_government = democratic }
		cost = 25
		days_remove = 90
		days_re_enable = 275
		ai_will_do = {
			factor = 1
			modifier = {
				congress_low_support_trigger = yes
				factor = 0
			}
		}
		modifier = { research_speed_factor = 0.08 }
		remove_effect = {
			USA_congress_medium_opposition = yes
		}
	}
	USA_invest_in_state_factory = {
		icon = generic_construction
		available = {
			custom_trigger_tooltip = {
				tooltip = USA_congress_invest_in_state_factory_tt
				any_owned_state = {
					has_state_flag = USA_congress_build_factory
					check_variable = { factory_goal = industrial_complex_level }
				}
			}
		}

		highlight_states = {
			highlight_states_trigger = {
				has_state_flag = USA_congress_build_factory
			}
		}

		days_mission_timeout = 360
		is_good = no
		activation = {
			any_owned_state = {
				has_state_flag = USA_congress_build_factory
			}
		}
		timeout_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_factory
				}
				clr_state_flag = USA_congress_build_factory
				clear_variable = factory_goal
			}
			
			USA_congress_small_opposition = yes 
			clr_country_flag = USA_congress_investment
		}
		complete_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_factory
				}
				clr_state_flag = USA_congress_build_factory
				clear_variable = factory_goal
			}
			ROOT = {
				USA_congress_medium_support = yes 
				clr_country_flag = USA_congress_investment
			}
			
		}
	}
	USA_invest_in_state_arms_factory = {
		icon = generic_construction
		available = {
			custom_trigger_tooltip = {
				tooltip = USA_congress_invest_in_state_arms_factory_tt
				any_owned_state = {
					has_state_flag = USA_congress_build_arms_factory
					check_variable = { arms_factory_goal = arms_factory_level }
				}
			}
		}

		highlight_states = {
			highlight_states_trigger = {
				has_state_flag = USA_congress_build_arms_factory
			}
		}


		days_mission_timeout = 360
		is_good = no
		activation = {
			any_owned_state = {
				has_state_flag = USA_congress_build_arms_factory
			}
		}
		timeout_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_arms_factory
				}
				clr_state_flag = USA_congress_build_arms_factory
				clear_variable = arms_factory_goal
			}
			
			USA_congress_small_opposition = yes 
			clr_country_flag = USA_congress_investment
		}
		complete_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_arms_factory
				}
				clr_state_flag = USA_congress_build_arms_factory
				clear_variable = arms_factory_goal
			}
			ROOT = {
				USA_congress_medium_support = yes 
				clr_country_flag = USA_congress_investment
			}
			
		}
	}
	USA_invest_in_state_dockyard = {
		icon = generic_construction
		available = {
			custom_trigger_tooltip = {
				tooltip = USA_congress_invest_in_dockyard_tooltip
				any_owned_state = {
					has_state_flag = USA_congress_build_dockyard
					check_variable = { dockyard_goal = building_level@dockyard }
				}
			}
		}

		highlight_states = {
			highlight_states_trigger = {
				has_state_flag = USA_congress_build_dockyard
			}
		}

		days_mission_timeout = 360
		is_good = no
		activation = {
			any_owned_state = {
				has_state_flag = USA_congress_build_dockyard
			}
		}
		timeout_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_dockyard
				}
				clr_state_flag = USA_congress_build_dockyard
				clear_variable = dockyard_goal
			}
			USA_congress_small_opposition = yes 
			clr_country_flag = USA_congress_investment
		}
		complete_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_dockyard
				}
				clr_state_flag = USA_congress_build_dockyard
				clear_variable = dockyard_goal
			}
			ROOT = {
				USA_congress_medium_support = yes 
				clr_country_flag = USA_congress_investment
			}
		}
	}
	USA_invest_in_state_infrastructure = {
		icon = generic_construction
		available = {
			custom_trigger_tooltip = {
				tooltip = USA_congress_invest_in_state_infrastructure_tt
				any_owned_state = {
					has_state_flag = USA_congress_build_infrastructure
					check_variable = { infrastructure_goal = infrastructure_level }
				}
			}
		}
		highlight_states = {
			highlight_states_trigger = {
				has_state_flag = USA_congress_build_infrastructure
			}
		}
		days_mission_timeout = 360
		is_good = no
		activation = {
			any_owned_state = {
				has_state_flag = USA_congress_build_infrastructure
			}
		}
		timeout_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_infrastructure
				}
				clr_state_flag = USA_congress_build_infrastructure
				clear_variable = infrastructure_goal
			}
			
			USA_congress_small_opposition = yes 
			clr_country_flag = USA_congress_investment
		}
		complete_effect = {
			random_owned_state = {
				limit = {
					has_state_flag = USA_congress_build_infrastructure
				}
				clr_state_flag = USA_congress_build_infrastructure
				clear_variable = infrastructure_goal
			}
			ROOT = {
				USA_congress_medium_support = yes 
				clr_country_flag = USA_congress_investment
			}
			
		}
	}
	USA_statehood_for_alaska = {
		icon = infiltrate_state
		available = {
			463 = {
				not = { is_core_of = ROOT }
			}
			political_power_daily > 0.1
		}
		visible = {
			463 = {
				is_fully_controlled_by = USA
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		fire_only_once = yes
		days_remove = 240
		cost = 25
		modifier = {
			political_power_gain = -0.1
		}
		remove_effect = {
			USA_congress_add_state = yes
			add_state_core = 463
			add_state_core = 650
			USA_calc_stars = yes
		}
	}
	USA_statehood_for_hawaii = {
		icon = infiltrate_state
		available = {
			629 = {
				not = { is_core_of = ROOT }
			}
			political_power_daily > 0.1
		}
		visible = {
			629 = {
				is_fully_controlled_by = USA
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		fire_only_once = yes
		days_remove = 240
		cost = 25
		modifier = {
			political_power_gain = -0.1
		}
		remove_effect = {
			USA_congress_add_state = yes
			add_state_core = 629
			USA_calc_stars = yes
		}
	}
	USA_statehood_for_puerto_rico = {
		icon = infiltrate_state
		available = {
			686 = {
				not = { is_core_of = ROOT }
			}
			political_power_daily > 0.1
		}
		visible = {
			686 = {
				is_fully_controlled_by = USA
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		fire_only_once = yes
		days_remove = 240
		cost = 25
		modifier = {
			political_power_gain = -0.1
		}
		remove_effect = {
			USA_congress_add_state = yes
			add_state_core = 686
			USA_calc_stars = yes
		}
	}
	USA_readmit_state = {
		icon = infiltrate_state
		available = {	
			political_power_daily > 0.1
		}
		visible = {
			any_owned_state = {
				is_core_of = CSA
				not = { is_core_of = ROOT }
				is_claimed_by = ROOT
			}
			has_completed_focus = USA_reintegration
		}
		ai_will_do = {
			factor = 1	
		}
		days_remove = 120
		cost = 25
		modifier = {
			political_power_gain = -0.1
		}
		complete_effect = {
			random_state = {
				limit = {
					is_controlled_by = ROOT
					is_core_of = CSA
					not = { is_core_of = ROOT }
					is_claimed_by = ROOT
				}
				set_state_flag = USA_state_admit
				custom_effect_tooltip = USA_readmit_state_tt
			}
		}
		remove_effect = {
			USA_congress_add_state = yes
			random_owned_state = {
				limit = { has_state_flag = USA_state_admit }
				ROOT = {
					add_state_core = PREV
				}
				clr_state_flag = USA_state_admit
			}			
		}
	}
	USA_reshuffle_congress = {
		available = {

		}
		visible = {
			has_completed_focus = USA_union_representation_act
		}
		days_re_enable = 180
		cost = 50
		complete_effect = {
			add_stability = -0.05
			USA_return_majority = yes
		}
	}
}