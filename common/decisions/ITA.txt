ITA_grind = {
	ITA_grind_ethiopia = {

		icon = generic_army_support

		available = {
			not = {has_war_with = ETH}
		}
		
		activation = {
			has_war_with = ETH	
		}
		
		is_good = no
		days_mission_timeout = 720

		timeout_effect = {
			country_event = { id = italy.17 }
		}
		
		complete_effect = {
			army_experience = 1
		}
	}
}